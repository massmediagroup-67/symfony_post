# MMG Post Bundle

## Installation
You can install it via composer:
  composer require mmg/post-bundle

## For using this bundle you have to add configuration files:
 * config/routes/post_bundle.yaml (required):
```     
post:
        resource: resource: "@PostBundle/Controller/PostController.php"        
        prefix: /
        type: annotation
```
 * config/packages/post_bundle.yaml  (required):
```   
      post:
        posts_number: 3
```