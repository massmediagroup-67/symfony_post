<?php

namespace mmg\PostBundle\Service;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;
use Doctrine\ORM\EntityManagerInterface;
use mmg\PostBundle\Entity\Comment;
use mmg\PostBundle\Entity\Post;

class FormHandler
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function handleForm(FormInterface $form, Request $request)
    {
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            $post = new Post();
            $post->setTitle($formData->getTitle());
            $post->setContent($formData->getContent());

            foreach ($formData->getComments() as $formComment) {
                $comment = new Comment();
                $comment->setAuthor($formComment->getAuthor());
                $comment->setContent($formComment->getContent());
                $comment->setPost($post);
                $post->setComment($comment);
                $this->em->persist($comment);
            }

            $this->em->persist($post);
            $this->em->flush();
            return true;
        }
        return false;
    }
}