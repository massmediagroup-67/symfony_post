<?php


namespace mmg\PostBundle\Controller;

use Doctrine\ORM\EntityManager;
use mmg\PostBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use mmg\PostBundle\Form\PostType;
use Symfony\Component\HttpFoundation\Request;
use mmg\PostBundle\Service\FormHandler;

class PostController extends AbstractController
{

    protected $postsNumber;
    protected $formHandler;

    public function __construct(int $postsNumber, FormHandler $formHandler)
    {
        $this->postsNumber = $postsNumber;
        $this->formHandler = $formHandler;
    }

    /**
     * @Route("/post", name="post")
     * @return Response
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm(PostType::class);
        if ($this->formHandler->handleForm($form, $request)) {
            $this->addFlash('success', 'Пост успішно створений');
            return $this->redirectToRoute('post');
        }

        $posts = $this->getDoctrine()->getRepository(Post::class)->getPosts($this->postsNumber);

        return $this->render('@Post/post/post.html.twig', ['form' => $form->createView(), 'posts' => $posts]);
    }
}