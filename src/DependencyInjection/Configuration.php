<?php


namespace mmg\PostBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('post');

        $treeBuilder->getRootNode()
            ->children()
                ->integerNode('posts_number')
                     ->min(1)
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}