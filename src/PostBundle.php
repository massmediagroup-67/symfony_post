<?php

namespace mmg\PostBundle;

use mmg\PostBundle\Repository\CommentRepository;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;


class PostBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->registerForAutoconfiguration(CommentRepository::class)
            ->addTag('doctrine.repository_service');
    }
}